package cn.code4java.springbok.dto;

import lombok.Data;

/**
 * @ClassName ItemSaleQueryDTO
 * @Description: ItemSaleQueryDTO
 * @Author fengwensheng
 * @Date 2023/12/21
 * @Version V1.0
 **/
@Data
public class ItemSaleQueryDTO extends BaseQueryDTO {

    /**
     * 销售商品名称
     */
    private String itemSaleName;
}
