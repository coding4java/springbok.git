package cn.code4java.springbok.dto;

import lombok.Data;

/**
 * @ClassName ItemSaleClassQueryDTO
 * @Description: ItemSaleClassQueryDTO
 * @Author fengwensheng
 * @Date 2023/12/21
 * @Version V1.0
 **/
@Data
public class ItemSaleClassQueryDTO extends BaseQueryDTO {

    /**
     * 营销分类id
     */
    private Integer itemSaleClassId;
    /**
     * 上级id
     */
    private Integer parentId;
    /**
     * 是否首页展示
     */
    private Boolean showIndex;
    /**
     * 门店id
     */
    private Integer sysBranchId;
}
