package cn.code4java.springbok.dto;

import cn.code4java.springbok.entity.CouponDirect;
import cn.code4java.springbok.entity.CouponDirectLine;
import lombok.Data;

import java.util.List;

/**
 * @ClassName CouponDTO
 * @Description: CouponDTO
 * @Author zhonghengtech
 * @Date 2023/11/25
 * @Version V1.0
 **/
@Data
public class CouponDirectDTO extends CouponDirect {

    private List<CouponDirectLine> couponDirectLineList;
}
