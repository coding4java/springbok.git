package cn.code4java.springbok.dto;

import lombok.Data;

/**
 * @ClassName CouponQueryDTO
 * @Description: CouponQueryDTO
 * @Author zhonghengtech
 * @Date 2023/12/21
 * @Version V1.0
 **/
@Data
public class CouponDirectQueryDTO extends BaseQueryDTO {

    /**
     * 发券名称
     */
    private String couponDirectName;
    /**
     * 状态 0 未开始 1 发送中 2 已完成 3 发送失败
     */
    private Integer status;
}
