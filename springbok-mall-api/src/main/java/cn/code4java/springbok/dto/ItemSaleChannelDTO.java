package cn.code4java.springbok.dto;

import lombok.Data;

import java.util.List;

/**
 * @ClassName ItemSaleChannelDTO
 * @Description: TODO
 * @Author xiehuangbao
 * @Date 2024/6/17
 * @Version V2.0.0
 **/
@Data
public class ItemSaleChannelDTO {
    /**
     * 渠道列表
     */
    private List<Integer> saleChannels;
    /**
     * 门店id列表
     */
    private List<Integer> sysBranchIds;
    /**
     * 销售商品id列表
     */
    private List<Integer> itemSaleIds;
}
