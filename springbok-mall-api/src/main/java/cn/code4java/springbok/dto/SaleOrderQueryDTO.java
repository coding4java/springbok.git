package cn.code4java.springbok.dto;

import lombok.Data;

/**
 * @ClassName SaleOrderQueryDTO
 * @Description: SaleOrderQueryDTO
 * @Author fengwensheng
 * @Date 2023/12/21
 * @Version V1.0
 **/
@Data
public class SaleOrderQueryDTO extends BaseQueryDTO {

    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 门店名称
     */
    private String branchName;
    /**
     * 订单状态
     * 1：待付款
     * 2：待发货
     * 3：待收货
     * 4：待评价
     * 5：已完成
     * 6：已取消
     */
    private Integer orderStatus;
    /**
     * 支付状态
     * 1：未支付
     * 2：已支付
     */
    private Integer payStatus;
    /**
     * 用户id
     */
    private Integer memberId;
}
