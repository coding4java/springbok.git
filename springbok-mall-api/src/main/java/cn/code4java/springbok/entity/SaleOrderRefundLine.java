package cn.code4java.springbok.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName SaleOrderRefundLine
 * @Description: 退货订单明细
 * @Author fengwensheng
 * @Date 2023/11/21
 * @Version V1.0
 **/
@Data
@TableName(value = "mall_sale_order_refund_line")
public class SaleOrderRefundLine {

    /**
     * 售后订单明细id
     */
    @TableId(type = IdType.AUTO)
    private Integer saleOrderRefundLineId;
    /**
     * 售后订单id
     */
    private Integer saleOrderRefundId;
}
