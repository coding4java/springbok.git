package cn.code4java.springbok.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName Item
 * @Description: 基础商品
 * @Author xiehuangbao
 * @Date 2024/07/12
 * @Version 2.0.2
 **/
@Data
@TableName(value = "mall_item")
public class Item extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer itemId;
    /**
     * 商品编码
     */
    private String itemCode;
    /**
     * 商品名称
     */
    private String itemName;
    /**
     * 基本单位名称
     */
    private String itemUnitName;
}
