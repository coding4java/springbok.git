package cn.code4java.springbok.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName ItemUnit
 * @Description: 基本单位
 * @Author xiehuangbao
 * @Date 2024/07/12
 * @Version 2.0.2
 **/
@Data
@TableName(value = "mall_item_unit")
public class ItemUnit extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer itemUnitId;
    /**
     * 单位名称
     */
    private String itemUnitName;
}
