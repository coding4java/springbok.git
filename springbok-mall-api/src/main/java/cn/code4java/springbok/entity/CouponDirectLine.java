package cn.code4java.springbok.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

/**
 * @ClassName CouponDirect
 * @Description: 定向发券选择优惠券
 * @Author zhonghengtech
 * @Date 2024/6/1
 * @Version V1.0
 **/
@Data
@TableName(value = "mall_coupon_direct_line")
public class CouponDirectLine {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer couponDirectLineId;
    /**
     * 定向发券id
     */
    private Integer couponDirectId;
    /**
     * 券id
     */
    private Integer couponId;
    /**
     * 发放数量
     */
    private Integer sendNum;
    /**
     * 有效期类型 1 指定日期 2 指定周期
     */
    private Integer validityType;
    /**
     * 开始日期
     */
    private Date startDate;
    /**
     * 结束日期
     */
    private Date endDate;
    /**
     * 发券后N天
     */
    private Integer afterDayNum;
    /**
     * 有效天数
     */
    private Integer validDayNum;
}
