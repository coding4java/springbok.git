package cn.code4java.springbok.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName ItemSaleChannel
 * @Description: 渠道商品
 * @Author xiehuangbao
 * @Date 2024/6/17
 * @Version V1.0
 **/
@Data
@TableName(value = "mall_item_sale_channel")
public class ItemSaleChannel extends BaseEntity {

    /**
     * 渠道商品id
     */
    @TableId(type = IdType.AUTO)
    private Integer itemSaleChannelId;
    /**
     * 门店id
     */
    private Integer sysBranchId;
    /**
     * 销售商品id
     */
    private Integer itemSaleId;
    /**
     * 渠道商品名称
     */
    private String itemSaleChannelName;
    /**
     * 销售渠道
     */
    private Integer saleChannel;
    /**
     * 状态
     * 1：上架
     * 2：下架
     */
    private Integer status;
}
