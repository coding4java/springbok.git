package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.ItemSaleChannelDTO;
import cn.code4java.springbok.dto.ItemSaleChannelQueryDTO;
import cn.code4java.springbok.entity.ItemSaleChannel;
import cn.code4java.springbok.vo.ItemSaleChannelVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @ClassName ItemSaleChannelService
 * @Description: 渠道商品服务类
 * @Author xiehuangbao
 * @Date 2024/6/17
 * @Version V2.0.0
 **/
public interface ItemSaleChannelService extends IService<ItemSaleChannel> {
    /**
     * 分页查询渠道商品
     *
     * @param itemSaleChannelQueryDTO
     * @return
     */
    Page<ItemSaleChannelVO> pageItemSaleChannel(ItemSaleChannelQueryDTO itemSaleChannelQueryDTO);

    /**
     * 新增渠道商品
     *
     * @param itemSaleChannelDTO
     * @return
     */
    int addItemSaleChannel(ItemSaleChannelDTO itemSaleChannelDTO);
}
