package cn.code4java.springbok.service;

import cn.code4java.springbok.entity.ItemUnit;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @ClassName ItemUnitService
 * @Description: 服务类
 * @Author xiehuangbao
 * @Date 2024/07/12
 * @Version 2.0.2
 **/
public interface ItemUnitService extends IService<ItemUnit> {
    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    Page<ItemUnit> pageItemUnit(ItemUnit params);
}
