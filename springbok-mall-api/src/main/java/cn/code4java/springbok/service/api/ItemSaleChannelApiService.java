package cn.code4java.springbok.service.api;

import cn.code4java.springbok.dto.ItemSaleChannelQueryDTO;
import cn.code4java.springbok.vo.ItemSaleChannelVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @ClassName ItemSaleChannelApiService
 * @Description: 渠道商品API服务类
 * @Author xiehuangbao
 * @Date 2024/6/18
 * @Version V2.0.0
 **/
public interface ItemSaleChannelApiService {
    /**
     * 分页查询渠道商品
     *
     * @param itemSaleChannelQueryDTO
     * @return
     */
    Page<ItemSaleChannelVO> pageItemSaleChannel(ItemSaleChannelQueryDTO itemSaleChannelQueryDTO);
    /**
     * 根据id查询渠道商品
     *
     * @param itemSaleChannelId
     * @return
     * @throws InterruptedException
     */
    ItemSaleChannelVO selectItemSaleChannelById(int itemSaleChannelId);
}
