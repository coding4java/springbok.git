package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.MemberCouponCreateDTO;
import cn.code4java.springbok.entity.MemberCoupon;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @ClassName MemberCouponService
 * @Description: 会员券服务类
 * @Author zhonghengtech
 * @Date 2024/6/5
 * @Version V1.0
 **/
public interface MemberCouponService extends IService<MemberCoupon> {

    /**
     * 创建会员券
     * @return
     */
    boolean createMemberCoupon(MemberCouponCreateDTO memberCouponCreateDTO);
}
