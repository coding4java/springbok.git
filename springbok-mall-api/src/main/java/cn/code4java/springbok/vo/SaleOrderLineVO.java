package cn.code4java.springbok.vo;

import cn.code4java.springbok.entity.SaleOrderLine;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName SaleOrderLineVO
 * @Description: SaleOrderLineVO
 * @Author fengwensheng
 * @Date 2024/7/15
 * @Version V2.0.0
 **/
@Data
public class SaleOrderLineVO extends SaleOrderLine {
    /**
     * 单位数量
     */
    private BigDecimal unitQuantity;
    /**
     * 商品编码
     */
    private String itemCode;
}
