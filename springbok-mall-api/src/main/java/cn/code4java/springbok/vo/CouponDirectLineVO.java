package cn.code4java.springbok.vo;

import cn.code4java.springbok.entity.CouponDirectLine;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName CouponDirectVO
 * @Description: CouponDirectVO
 * @Author zhonghengtech
 * @Date 2024/06/13
 * @Version V1.0
 **/
@Data
public class CouponDirectLineVO extends CouponDirectLine {
    /**
     * 券名称
     */
    private String couponName;
    /**
     * 券门槛
     */
    private BigDecimal couponLimit;
    /**
     * 券抵扣金额，折扣券填(0-1)区间的数字，例如0.88代表88折
     */
    private BigDecimal couponAmount;
}
