package cn.code4java.springbok.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.util.Date;

/**
 * @ClassName MemberVO
 * @Description: 会员VO
 * @Author fengwensheng
 * @Date 2023/11/25
 * @Version V1.0
 **/
@Data
@HeadRowHeight(30)
@ContentRowHeight(20)
public class MemberVO {

    /**
     * 用户名
     */
    @ExcelProperty("用户名")
    private String username;
    /**
     * 会员昵称
     */
    @ExcelProperty("会员昵称")
    private String nickname;
    /**
     * 会员手机号
     */
    @ExcelProperty("会员手机号")
    private String phone;
    /**
     * 性别
     */
    @ExcelProperty("性别")
    private String gender;
    /**
     * 生日
     */
    @ExcelProperty("生日")
    private String birthday;
    /**
     * 省市区
     */
    @ExcelProperty("省市区")
    private String location;
    /**
     * 创建时间
     */
    @DateTimeFormat("yyyy年MM月dd日HH时mm分ss秒")
    @ExcelProperty("创建时间")
    private Date createdTime;
}
