package cn.code4java.springbok.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName ItemSaleChannelClassVO
 * @Description: 分类下的渠道商品VO
 * @Author xiehuangbao
 * @Date 2024/6/17
 * @Version V1.0
 **/
@Data
public class ItemSaleChannelClassVO {

    /**
     * 渠道商品id
     */
    private Integer itemSaleChannelId;

    /**
     * 商品编码
     */
    private String itemCode;

    /**
     * 渠道商品名称
     */
    private String itemSaleChannelName;

    /**
     * 销售价格
     */
    private BigDecimal itemSalePrice;

    /**
     * 商品主图
     */
    private String mainImage;
}
