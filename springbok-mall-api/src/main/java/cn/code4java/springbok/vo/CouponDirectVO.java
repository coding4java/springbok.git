package cn.code4java.springbok.vo;

import cn.code4java.springbok.entity.CouponDirect;
import cn.code4java.springbok.entity.CouponDirectLine;
import lombok.Data;

import java.util.List;

/**
 * @ClassName CouponDirectVO
 * @Description: CouponDirectVO
 * @Author zhonghengtech
 * @Date 2024/06/13
 * @Version V1.0
 **/
@Data
public class CouponDirectVO extends CouponDirect {

    private List<CouponDirectLineVO> couponDirectLineList;
}
