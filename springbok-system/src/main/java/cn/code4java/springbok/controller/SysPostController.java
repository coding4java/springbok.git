package cn.code4java.springbok.controller;

import cn.code4java.springbok.annotation.Log;
import cn.code4java.springbok.dto.SysPostQueryDTO;
import cn.code4java.springbok.dto.SysRoleDTO;
import cn.code4java.springbok.entity.SysPost;
import cn.code4java.springbok.entity.SysRole;
import cn.code4java.springbok.entity.SysRoleMenu;
import cn.code4java.springbok.service.SysPostService;
import cn.code4java.springbok.vo.BaseResponse;
import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName SysPostController
 * @Description: 岗位管理
 * @Author fengwensheng
 * @Date 2024/03/13
 * @Version V1.0
 **/
@Tag(name = "岗位管理")
@RestController
@RequiredArgsConstructor
@RequestMapping("/post")
public class SysPostController {

    private final SysPostService sysPostService;

    /**
     * 分页查询岗位信息
     *
     * @param params
     * @return
     */
    @GetMapping("/pageSysPost")
    @Operation(summary = "分页查询岗位信息", description = "分页查询岗位信息")
    public BaseResponse pageSysPost(SysPostQueryDTO params) {
        return BaseResponse.success(sysPostService.pageSysPost(params));
    }

    /**
     * 查询岗位信息列表
     *
     * @return
     */
    @GetMapping("/listSysPost")
    @Operation(summary = "查询岗位信息列表", description = "查询岗位信息列表")
    public BaseResponse listSysPost() {
        return BaseResponse.success(sysPostService.listSysPost());
    }

    /**
     * 根据id查询岗位信息
     *
     * @param sysPostId
     * @return
     */
    @GetMapping("/selectSysPostById")
    @Operation(summary = "根据id查询岗位信息", description = "根据id查询岗位信息")
    public BaseResponse selectSysPostById(int sysPostId) {
        return BaseResponse.success(sysPostService.selectSysPostById(sysPostId));
    }

    /**
     * 新增岗位
     *
     * @param sysPost
     * @return
     */
    @Log(title = "新增岗位")
    @PostMapping("/addSysPost")
    @Operation(summary = "新增岗位", description = "新增岗位")
    public BaseResponse addSysPost(@RequestBody SysPost sysPost) {
        sysPostService.addSysPost(sysPost);
        return BaseResponse.success();
    }

    /**
     * 修改岗位
     *
     * @param sysPost
     * @return
     */
    @Log(title = "修改岗位")
    @PostMapping("/updateSysPost")
    @Operation(summary = "修改岗位", description = "修改岗位")
    public BaseResponse updateSysPost(@RequestBody SysPost sysPost) {
        sysPostService.updateSysPost(sysPost);
        return BaseResponse.success();
    }

    /**
     * 删除岗位
     *
     * @param sysPostId
     * @return
     */
    @Log(title = "删除岗位")
    @PostMapping("/deleteSysPost")
    @Operation(summary = "删除岗位", description = "删除岗位")
    public BaseResponse deleteSysPost(@Parameter(description = "岗位id", required = true) Integer sysPostId) {
        sysPostService.deleteSysPost(sysPostId);
        return BaseResponse.success();
    }
}
