package cn.code4java.springbok.controller;

import cn.code4java.springbok.dto.TableDTO;
import cn.code4java.springbok.entity.Table;
import cn.code4java.springbok.service.TableService;
import cn.code4java.springbok.vo.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;


/**
 * @ClassName TableController
 * @Description: 生成表
 * @Author xiehuangbao
 * @Date 2024/07/09
 * @Version 2.0.0
 **/
@Tag(name = "表管理")
@RestController
@RequiredArgsConstructor
@RequestMapping("/table")
public class TableController {

    private final TableService tableService;

    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @GetMapping("/pageTable")
    @Operation(summary = "分页查询", description = "分页查询")
    public BaseResponse pageTable(TableDTO params) {
        return BaseResponse.success(tableService.pageTable(params));
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @GetMapping("/selectTableById")
    @Operation(summary = "根据id查询商品信息", description = "根据id查询商品信息")
    public BaseResponse selectTableById(@Parameter(description = "id", required = true) Integer id) {
        return BaseResponse.success(tableService.selectTableById(id));
    }

    /**
     * 新增
     *
     * @param tableDTO
     * @return
     */
    @PostMapping("/addTable")
    public BaseResponse addTable(@RequestBody TableDTO tableDTO) {
        return BaseResponse.success(tableService.addTable(tableDTO));
    }

    /**
     * 修改
     *
     * @param tableDTO
     * @return
     */
    @PostMapping("/updateTable")
    public BaseResponse updateTable(@RequestBody TableDTO tableDTO) {
        return BaseResponse.success(tableService.updateTable(tableDTO));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @PostMapping("/deleteTable")
    public BaseResponse deleteTable(@Parameter(description = "id", required = true) Integer id) {
        return BaseResponse.success(tableService.deleteTable(id));
    }

    /**
     * 生成代码zip压缩包
     *
     * @param id
     * @param response
     * @throws Exception
     */
    @GetMapping("/generateCode")
    public void generateCode(@Parameter(description = "id", required = true) Integer id, HttpServletResponse response) throws Exception {
        tableService.generateCode(id, response);
    }
}
