package cn.code4java.springbok.mapper;

import cn.code4java.springbok.dto.SysBranchQueryDTO;
import cn.code4java.springbok.entity.SysBranch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * @ClassName SysBranchMapper
 * @Description: SysBranchMapper
 * @Author xiehuangbao
 * @Date 2024/6/15
 * @Version V1.0
 **/
public interface SysBranchMapper extends BaseMapper<SysBranch> {
    Page<SysBranch> pageSysBranch(Page page, @Param(value = "query") SysBranchQueryDTO sysBranchQueryDTO);
}
