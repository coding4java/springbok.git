package cn.code4java.springbok.service;

import cn.code4java.springbok.mapper.IndexMapper;
import cn.code4java.springbok.vo.ItemSaleStatisticsVO;
import cn.code4java.springbok.vo.StatisticsVO;
import cn.code4java.springbok.constant.SpringbokConstant;
import cn.code4java.springbok.utils.RedisUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @ClassName IndexServiceImpl
 * @Description: 首页服务实现类
 * @Author fengwensheng
 * @Date 2024/2/4
 * @Version V1.0
 **/
@Service
@AllArgsConstructor
public class IndexServiceImpl implements IndexService {

    private IndexMapper indexMapper;

    /**
     * 首页数据统计
     *
     * @return
     */
    @Override
    public StatisticsVO statistics() {
        StatisticsVO statistics = indexMapper.statistics();
        // 销售额前10商品
        List<ItemSaleStatisticsVO> itemSaleStatisticsVOS = indexMapper.selectTopItemSaleStatisticsByMonth();
        statistics.setTopItemSaleStatisticsByMonth(itemSaleStatisticsVOS);
        return statistics;
    }
}
