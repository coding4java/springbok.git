package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.SysPostQueryDTO;
import cn.code4java.springbok.entity.SysPost;
import cn.code4java.springbok.mapper.SysPostMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName SysPostServiceImpl
 * @Description: 岗位服务实现类
 * @Author fengwensheng
 * @Date 2024/03/13
 * @Version V1.0
 **/
@Service
@AllArgsConstructor
public class SysPostServiceImpl extends BaseServiceImpl<SysPostMapper, SysPost> implements SysPostService {

    private SysPostMapper sysPostMapper;

    /**
     * 分页查询岗位
     *
     * @param sysPostQueryDTO
     * @return
     */
    @Override
    public Page<SysPost> pageSysPost(SysPostQueryDTO sysPostQueryDTO) {
        LambdaQueryWrapper<SysPost> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.isNotBlank(sysPostQueryDTO.getSysPostName()), SysPost::getSysPostName, sysPostQueryDTO.getSysPostName());
        return sysPostMapper.selectPage(getPage(), queryWrapper);
    }

    /**
     * 查询岗位列表
     *
     * @return
     */
    @Override
    public List<SysPost> listSysPost() {
        return sysPostMapper.selectList(new LambdaQueryWrapper<>());
    }

    /**
     * 根据id查询岗位
     *
     * @param sysPostId
     * @return
     */
    @Override
    public SysPost selectSysPostById(int sysPostId) {
        return sysPostMapper.selectById(sysPostId);
    }

    /**
     * 新增岗位
     *
     * @param sysPost
     * @return
     */
    @Override
    public int addSysPost(SysPost sysPost) {
        return sysPostMapper.insert(sysPost);
    }

    /**
     * 修改岗位
     *
     * @param sysPost
     * @return
     */
    @Override
    public int updateSysPost(SysPost sysPost) {
        return sysPostMapper.updateById(sysPost);
    }

    /**
     * 删除岗位
     *
     * @param sysPostId
     * @return
     */
    @Override
    public int deleteSysPost(int sysPostId) {
        return sysPostMapper.deleteById(sysPostId);
    }
}
