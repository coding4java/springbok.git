package cn.code4java.springbok.controller;

import cn.code4java.springbok.dto.CouponDirectDTO;
import cn.code4java.springbok.dto.CouponDirectQueryDTO;
import cn.code4java.springbok.service.CouponDirectService;
import cn.code4java.springbok.vo.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName CouponDirectController
 * @Description: 定向发券控制器
 * @Author zhonghengtech
 * @Date 2024/6/1
 * @Version V1.0
 **/
@Tag(name = "定向发券管理")
@RestController
@RequiredArgsConstructor
@RequestMapping("/couponDirect")
public class CouponDirectController {

    private final CouponDirectService couponDirectService;

    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @GetMapping("/pageCouponDirect")
    @Operation(summary = "分页查询", description = "分页查询")
    public BaseResponse pageCouponDirect(CouponDirectQueryDTO params) {
        return BaseResponse.success(couponDirectService.pageCouponDirect(params));
    }

    /**
     * 根据id查询定向发券
     *
     * @param couponDirectId
     * @return
     */
    @GetMapping("/selectCouponDirectById")
    @Operation(summary = "根据id查询定向发券", description = "根据id查询定向发券")
    public BaseResponse selectCouponDirectById(Integer couponDirectId) {
        return BaseResponse.success(couponDirectService.selectCouponDirectById(couponDirectId));
    }

    /**
     * 新增定向发券
     *
     * @param couponDirectDTO
     * @return
     */
    @PostMapping("/addCouponDirect")
    @Operation(summary = "新增定向发券", description = "新增定向发券")
    public BaseResponse addCouponDirect(@RequestBody CouponDirectDTO couponDirectDTO) {
        return BaseResponse.success(couponDirectService.addCouponDirect(couponDirectDTO));
    }

    /**
     * 修改定向发券
     *
     * @param couponDirectDTO
     * @return
     */
    @PostMapping("/updateCouponDirect")
    @Operation(summary = "修改定向发券", description = "修改定向发券")
    public BaseResponse updateCouponDirect(@RequestBody CouponDirectDTO couponDirectDTO) {
        return BaseResponse.success(couponDirectService.updateCouponDirect(couponDirectDTO));
    }

    /**
     * 删除定向发券
     *
     * @param couponDirectId
     * @return
     */
    @PostMapping("/deleteCouponDirect")
    @Operation(summary = "删除定向发券", description = "删除定向发券")
    public BaseResponse deleteCouponDirect(Integer couponDirectId) {
        return BaseResponse.success(couponDirectService.deleteCouponDirect(couponDirectId));
    }

    /**
     * 发券
     *
     * @param couponDirectId
     * @return
     */
    @PostMapping("/sendCoupon")
    @Operation(summary = "发券", description = "发券")
    public BaseResponse sendCoupon(Integer couponDirectId) {
        return BaseResponse.success(couponDirectService.sendCoupon(couponDirectId));
    }
}
