package cn.code4java.springbok.controller;

import cn.code4java.springbok.dto.ItemSaleChannelDTO;
import cn.code4java.springbok.dto.ItemSaleChannelQueryDTO;
import cn.code4java.springbok.entity.ItemSaleChannel;
import cn.code4java.springbok.service.ItemSaleChannelService;
import cn.code4java.springbok.utils.ListUtils;
import cn.code4java.springbok.vo.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName ItemSaleChannelController
 * @Description: 渠道商品控制器
 * @Author xiehuangbao
 * @Date 2024/6/17
 * @Version V2.0.0
 **/
@Tag(name = "渠道商品管理")
@RestController
@RequiredArgsConstructor
@RequestMapping("/itemSaleChannel")
public class ItemSaleChannelController {

    private final ItemSaleChannelService itemSaleChannelService;

    /**
     * 分页查询渠道商品
     *
     * @param params
     * @return
     */
    @GetMapping("/pageItemSaleChannel")
    @Operation(summary = "分页查询渠道商品", description = "分页查询渠道商品")
    public BaseResponse pageItemSaleChannel(ItemSaleChannelQueryDTO params) {
        return BaseResponse.success(itemSaleChannelService.pageItemSaleChannel(params));
    }

    /**
     * 新增渠道商品
     *
     * @param itemSaleChannelDTO
     * @return
     */
    @PostMapping("/addItemSaleChannel")
    @Operation(summary = "新增渠道商品", description = "新增渠道商品")
    public BaseResponse addItemSale(@RequestBody ItemSaleChannelDTO itemSaleChannelDTO) {
        return BaseResponse.success(itemSaleChannelService.addItemSaleChannel(itemSaleChannelDTO));
    }

    /**
     * 修改渠道商品
     *
     * @param itemSaleChannel
     * @return
     */
    @PostMapping("/updateItemSaleChannel")
    @Operation(summary = "修改渠道商品", description = "修改渠道商品")
    public BaseResponse updateItemSaleChannel(@RequestBody ItemSaleChannel itemSaleChannel) {
        return BaseResponse.success(itemSaleChannelService.updateById(itemSaleChannel));
    }

    /**
     * 删除渠道商品
     *
     * @param ids
     * @return
     */
    @PostMapping("/deleteItemSaleChannel")
    @Operation(summary = "删除渠道商品", description = "删除渠道商品")
    public BaseResponse deleteItemSaleChannel(@RequestParam("ids") String ids) {
        return BaseResponse.success(itemSaleChannelService.removeByIds(ListUtils.toListAsInt(ids.split(","))));
    }
}
