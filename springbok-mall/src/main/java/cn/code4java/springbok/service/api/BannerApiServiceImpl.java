package cn.code4java.springbok.service.api;

import cn.code4java.springbok.entity.Banner;
import cn.code4java.springbok.mapper.BannerMapper;
import cn.code4java.springbok.service.BaseServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @ClassName BannerApiServiceImpl
 * @Description: 广告API服务实现类
 * @Author fengwensheng
 * @Date 2023/11/22
 * @Version V1.0
 **/
@Service
@AllArgsConstructor
public class BannerApiServiceImpl extends BaseServiceImpl<BannerMapper, Banner> implements BannerApiService {
}
