package cn.code4java.springbok.service.api;

import cn.code4java.springbok.entity.*;
import cn.code4java.springbok.exception.BusinessException;
import cn.code4java.springbok.exception.ExceptionEnum;
import cn.code4java.springbok.mapper.ItemSaleChannelMapper;
import cn.code4java.springbok.mapper.ItemSaleMapper;
import cn.code4java.springbok.mapper.ItemSaleSkuMapper;
import cn.code4java.springbok.mapper.MemberCartMapper;
import cn.code4java.springbok.service.BaseServiceImpl;
import cn.code4java.springbok.service.SysBranchService;
import cn.code4java.springbok.utils.ListUtils;
import cn.code4java.springbok.vo.MemberCartVO;
import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName MemberCartApiServiceImpl
 * @Description: 购物车API服务实现类
 * @Author fengwensheng
 * @Date 2023/11/22
 * @Version V1.0
 **/
@Service
@AllArgsConstructor
public class MemberCartApiServiceImpl extends BaseServiceImpl<MemberCartMapper, MemberCart> implements MemberCartApiService {

    private MemberCartMapper memberCartMapper;
    private ItemSaleMapper itemSaleMapper;
    private ItemSaleSkuMapper itemSaleSkuMapper;
    private ItemSaleChannelMapper itemSaleChannelMapper;
    private SysBranchService sysBranchService;

    /**
     * 查询会员购物车列表
     *
     * @param memberCart
     * @return
     */
    @Override
    public List<MemberCartVO> listMemberCart(MemberCart memberCart) {
        memberCart.setMemberId(StpUtil.getLoginIdAsInt());
        return memberCartMapper.listMemberCart(memberCart);
    }

    /**
     * 根据多个id查询购物车
     *
     * @param ids
     * @return
     */
    @Override
    public List<MemberCartVO> listMemberCartByIds(String ids) {
        return memberCartMapper.listMemberCartByIds(ListUtils.toListAsInt(ids.split(",")));
    }

    /**
     * 新增购物车商品
     *
     * @param memberCart
     * @return
     */
    @Override
    public Integer addMemberCart(MemberCart memberCart) {
        ItemSaleSku itemSaleSku = itemSaleSkuMapper.selectById(memberCart.getItemSaleSkuId());
        if (itemSaleSku == null) {
            throw new BusinessException(ExceptionEnum.BUSINESS_DATA_ABSENT_ERROR, "商品规格不存在");
        }
        ItemSale itemSale = itemSaleMapper.selectById(itemSaleSku.getItemSaleId());
        if (itemSale == null) {
            throw new BusinessException(ExceptionEnum.BUSINESS_DATA_ABSENT_ERROR, "商品不存在");
        }
        ItemSaleChannel itemSaleChannel = itemSaleChannelMapper.selectById(memberCart.getItemSaleChannelId());
        if (itemSaleChannel == null || itemSaleChannel.getStatus() == 2) {
            throw new BusinessException(ExceptionEnum.BUSINESS_DATA_ABSENT_ERROR, "商品不存在或未上架");
        }
        SysBranch sysBranch = sysBranchService.getById(itemSaleChannel.getSysBranchId());
        if (sysBranch == null || sysBranch.getBranchStatus() == 2) {
            throw new BusinessException(ExceptionEnum.BUSINESS_DATA_ABSENT_ERROR, "商品所在门店不存在或已关闭");
        }
        MemberCart cart = memberCartMapper.selectOne(Wrappers.<MemberCart>lambdaQuery()
                .eq(MemberCart::getItemSaleChannelId, memberCart.getItemSaleChannelId())
                .eq(MemberCart::getItemSaleSkuId, memberCart.getItemSaleSkuId())
                .eq(MemberCart::getMemberId, StpUtil.getLoginIdAsInt()));
        if (cart != null) {
            cart.setNumber(cart.getNumber().add(memberCart.getNumber()));
            memberCartMapper.updateById(cart);
            return cart.getMemberCartId();
        }
        memberCart.setBranchName(sysBranch.getBranchName());
        memberCart.setMemberId(StpUtil.getLoginIdAsInt());
        memberCart.setItemSaleId(itemSale.getItemSaleId());
        memberCart.setItemSaleName(itemSale.getItemSaleName());
        memberCart.setAddPrice(itemSaleSku.getPrice());
        memberCart.setAttrsText(itemSaleSku.getSpec());
        memberCartMapper.insert(memberCart);
        return memberCart.getMemberCartId();
    }

    /**
     * 修改购物车商品
     *
     * @param memberCart
     * @return
     */
    @Override
    public boolean updateMemberCart(MemberCart memberCart) {
        MemberCart updateMemberCart = new MemberCart();
        updateMemberCart.setNumber(memberCart.getNumber());
        return this.update(updateMemberCart, new LambdaQueryWrapper<MemberCart>()
                .eq(MemberCart::getItemSaleChannelId, memberCart.getItemSaleChannelId())
                .eq(MemberCart::getMemberId, StpUtil.getLoginIdAsInt())
                .eq(MemberCart::getItemSaleSkuId, memberCart.getItemSaleSkuId()));
    }

    /**
     * 删除购物车商品
     *
     * @param ids
     * @return
     */
    @Override
    public Integer deleteMemberCart(String ids) {
        return memberCartMapper.deleteBatchIds(Arrays.asList(ids.split(",")));
    }
}
