package cn.code4java.springbok.mapper;

import cn.code4java.springbok.dto.ItemSaleChannelQueryDTO;
import cn.code4java.springbok.entity.ItemSaleChannel;
import cn.code4java.springbok.vo.ItemSaleChannelClassVO;
import cn.code4java.springbok.vo.ItemSaleChannelSkuVO;
import cn.code4java.springbok.vo.ItemSaleChannelVO;
import cn.code4java.springbok.vo.ItemSaleSkuVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @ClassName ItemSaleChannelMapper
 * @Description: ItemSaleChannelMapper
 * @Author xiehuangbao
 * @Date 2024/6/17
 * @Version V2.0.0
 **/
public interface ItemSaleChannelMapper extends BaseMapper<ItemSaleChannel> {
    Page<ItemSaleChannelVO> pageItemSaleChannel(Page page, @Param(value = "query") ItemSaleChannelQueryDTO itemSaleChannelQueryDTO);
    List<ItemSaleChannelClassVO> selectItemSaleChannelByClassCode(@Param(value = "itemSaleClassCode") String itemSaleClassCode, @Param(value = "sysBranchId") int sysBranchId);
    ItemSaleChannelVO selectItemSaleChannelById(int itemSaleChannelId);
    List<ItemSaleChannelSkuVO> selectItemSaleChannelSkuById(int itemSaleChannelId);
}
