package cn.code4java.springbok.mapper;

import cn.code4java.springbok.entity.CouponDirectLine;
import cn.code4java.springbok.vo.CouponDirectLineVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @ClassName CouponDirectMapper
 * @Description: CouponDirectMapper
 * @Author zhonghengtech
 * @Date 2024/6/1
 * @Version V1.0
 **/
public interface CouponDirectLineMapper extends BaseMapper<CouponDirectLine> {
    List<CouponDirectLineVO> selectCouponDirectLine(int couponDirectId);
}
