package cn.code4java.springbok.mapper;

import cn.code4java.springbok.entity.ItemUnit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @ClassName ItemUnitMapper
 * @Description: Mapper类
 * @Author xiehuangbao
 * @Date 2024/07/12
 * @Version 2.0.2
 **/
public interface ItemUnitMapper extends BaseMapper<ItemUnit> {
}
