package cn.code4java.springbok.mapper;

import cn.code4java.springbok.entity.ItemSaleClass;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @ClassName ItemSaleClassMapper
 * @Description: ItemSaleClassMapper
 * @Author fengwensheng
 * @Date 2024/02/21
 * @Version V1.0
 **/
public interface ItemSaleClassMapper extends BaseMapper<ItemSaleClass> {
}
