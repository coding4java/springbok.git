package cn.code4java.springbok.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName SysBranchQueryDTO
 * @Description: SysBranchQueryDTO
 * @Author fengwensheng
 * @Date 2023/12/21
 * @Version V1.0
 **/
@Data
@Schema(title = "门店查询参数")
public class SysBranchQueryDTO extends BaseQueryDTO {

    /**
     * 门店名称
     */
    private String branchName;
    /**
     * 门店状态
     * 1：闭店
     * 2：营业
     */
    private Integer branchStatus;
}
