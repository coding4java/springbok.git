package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.SysBranchQueryDTO;
import cn.code4java.springbok.entity.SysBranch;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @ClassName SysUserService
 * @Description: 门店服务类
 * @Author xiehuangbao
 * @Date 2024/06/15
 * @Version V1.0
 **/
public interface SysBranchService extends IService<SysBranch> {
    Page<SysBranch> pageSysBranch(SysBranchQueryDTO params);
}
