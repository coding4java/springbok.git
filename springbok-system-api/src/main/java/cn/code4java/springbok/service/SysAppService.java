package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.SysAppQueryDTO;
import cn.code4java.springbok.entity.SysApp;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @ClassName SysAppService
 * @Description: 应用服务类
 * @Author fengwensheng
 * @Date 2024/2/23
 * @Version V1.0
 **/
public interface SysAppService extends IService<SysApp> {
    Page<SysApp> pageSysApp(SysAppQueryDTO params);
}
