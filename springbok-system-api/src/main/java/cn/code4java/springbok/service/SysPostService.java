package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.SysPostQueryDTO;
import cn.code4java.springbok.entity.SysPost;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @ClassName SysPostService
 * @Description: 岗位服务类
 * @Author fengwensheng
 * @Date 2024/03/13
 * @Version V1.0
 **/
public interface SysPostService extends IService<SysPost> {
    /**
     * 分页查询岗位
     *
     * @param sysPostQueryDTO
     * @return
     */
    Page<SysPost> pageSysPost(SysPostQueryDTO sysPostQueryDTO);

    /**
     * 查询岗位列表
     *
     * @return
     */
    List<SysPost> listSysPost();

    /**
     * 根据id查询岗位
     *
     * @param sysPostId
     * @return
     */
    SysPost selectSysPostById(int sysPostId);

    /**
     * 新增岗位
     *
     * @param sysPost
     * @return
     */
    int addSysPost(SysPost sysPost);

    /**
     * 修改岗位
     *
     * @param sysPost
     * @return
     */
    int updateSysPost(SysPost sysPost);

    /**
     * 删除岗位
     *
     * @param sysPostId
     * @return
     */
    int deleteSysPost(int sysPostId);
}
