package cn.code4java.springbok.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName SysBranch
 * @Description: 门店
 * @Author xiehuangbao
 * @Date 2024/6/15
 * @Version V1.0
 **/
@Data
@TableName(value = "sys_branch")
public class SysBranch extends BaseEntity {
    /**
     * 门店id
     */
    @TableId(type = IdType.AUTO)
    private Integer sysBranchId;
    /**
     * 门店编码
     */
    private String branchCode;
    /**
     * 门店名称
     */
    private String branchName;
    /**
     * 门店联系电话
     */
    private String branchTel;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区
     */
    private String district;
    /**
     * 门店详细地址
     */
    private String branchAddress;
    /**
     * 邮政编码
     */
    private String postCode;
    /**
     * 门店状态
     * 1：营业
     * 2：闭店
     */
    private Integer branchStatus;
}
