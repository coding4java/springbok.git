package cn.code4java.springbok.vo;

import lombok.Data;

/**
 * @ClassName CecheIndexVO
 * @Description: CecheIndexVO
 * @Author fengwensheng
 * @Date 2024/8/2
 * @Version V2.0.3
 **/
@Data
public class CecheIndexVO {

    /**
     * 版本号
     */
    private String redisVersion;
    /**
     * Redis启动后的天数
     */
    private String uptimeInDays;
    /**
     * 当前客户端连接数
     */
    private String connectedClients;
    /**
     * 已使用内存
     */
    private String usedMemoryHuman;
    /**
     * 数据集已使用内存
     */
    private String usedMemoryDataset;
    /**
     * 当前总内存
     */
    private String totalSystemMemoryHuman;
    /**
     * 当前RDB的进行状态
     */
    private String rdbBgsaveInProgress;
    /**
     * 上次成功完成的RDB时间戳
     */
    private String rdbLastSaveTime;
    /**
     * 上次RDB的状态
     */
    private String rdbLastBgsaveStatu;
    /**
     * 上次RDB保存用时（秒）
     */
    private String rdbLastBgsaveTimeSec;
    /**
     * 启动以来执行RDB快照的次数
     */
    private String rdbSaves;
    /**
     * 是否开启AOF
     */
    private String aofEnabled;
    /**
     * 正在进行AOF重写的状态
     */
    private String aofRewireInProgress;
    /**
     * 上次AOF重写用时（秒）
     */
    private String aofLastRewireTimeSec;
    /**
     * 上次AOF重写的状态
     */
    private String aofLastBgrewriteStatus;
    /**
     * AOF当前文件大小
     */
    private String aofCurrentSize;
    /**
     * 启动以来执行AOF重写的次数
     */
    private String aofRewrites;
}
