package cn.code4java.springbok.service;

import cn.code4java.springbok.constant.SpringbokConstant;
import cn.code4java.springbok.utils.ServletUtils;
import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @ClassName BaseService
 * @Description: TODO
 * @Author fengwensheng
 * @Date 2024/7/5
 * @Version V2.0.0
 **/
public class BaseServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> {

    public LambdaQueryWrapper<T> getLambdaQuery(Object o) {
        return Wrappers.lambdaQuery(BeanUtil.toBean(o, this.getEntityClass()));
    }

    public LambdaQueryWrapper getLambdaQuery(Object o, Class clazz) {
        return Wrappers.lambdaQuery(BeanUtil.toBean(o, clazz));
    }

    public QueryWrapper<T> getQuery(Object o) {
        return Wrappers.query(BeanUtil.toBean(o, this.getEntityClass()));
    }

    public Page<T> getPage() {
        Long current = ServletUtils.getParameterToLong(SpringbokConstant.PAGE_FIELD_CURRENT, 1L);
        Long size = ServletUtils.getParameterToLong(SpringbokConstant.PAGE_FIELD_SIZE, SpringbokConstant.PAGE_SIZE_DEFAULT);
        Page<T> page = new Page<>();
        page.setCurrent(current);
        page.setSize(size);
        return page;
    }
}
