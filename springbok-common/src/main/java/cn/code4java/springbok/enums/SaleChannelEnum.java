package cn.code4java.springbok.enums;

/**
 * @ClassName SaleChannelEnum
 * @Description: 平台渠道枚举
 * @Author xiehuangbao
 * @Date 2024/6/17
 * @Version V1.0
 **/
public enum SaleChannelEnum {

    WX(1, "微信小程序"),
    MT(2, "美团"),
    ELM( 3, "饿了么");

    private int channel;
    private String desc;

    SaleChannelEnum(int channel, String desc) {
        this.channel = channel;
        this.desc = desc;
    }

    public int getChannel() {
        return channel;
    }
}
