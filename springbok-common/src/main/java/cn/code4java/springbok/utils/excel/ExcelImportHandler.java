package cn.code4java.springbok.utils.excel;

import java.util.List;

/**
 * @ClassName ExcelImportHandler
 * @Description: TODO
 * @Author fengwensheng
 * @Date 2024/7/6
 * @Version V2.0.0
 **/
public interface ExcelImportHandler<T> {
    /**
     * 处理分段数据
     *
     * @param data 分段数据
     */
    void handle(List<T> data);
}
