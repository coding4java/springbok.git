package cn.code4java.springbok.config;

import cn.code4java.springbok.enums.GenTemplateEnum;
import cn.code4java.springbok.enums.ModeEnum;

/**
 * @ClassName GenTemplateConfig
 * @Description: 生成模板配置
 * @Author fengwensheng
 * @Date 2024/7/9
 * @Version V2.0.0
 **/
public class GenTemplateConfig {
    /**
     * 代码生成路径
     */
    private String outPath;
    /**
     * 字段命名方式
     */
    private String mode;
    /**
     * 作者
     */
    private String author;
    /**
     * 版本号
     */
    private String version;
    /**
     * 包名
     */
    private String packageName;
    /**
     * sql目录
     */
    private DirConfig sqlDir;
    /**
     * 实体类目录
     */
    private DirConfig entityDir;
    /**
     * DTO类目录
     */
    private DirConfig dtoDir;
    /**
     * VO类目录
     */
    private DirConfig voDir;
    /**
     * 控制器目录
     */
    private DirConfig controllerDir;
    /**
     * 服务类目录
     */
    private DirConfig serviceDir;
    /**
     * 服务实现类目录
     */
    private DirConfig serviceImplDir;
    /**
     * Mapper目录
     */
    private DirConfig mapperDir;
    /**
     * Mapper XML文件目录
     */
    private DirConfig mapperXmlDir;
    /**
     * 前端API文件类目录
     */
    private DirConfig apiJsDir;
    /**
     * 前端Vue文件类目录
     */
    private DirConfig vue3Dir;

    private GenTemplateConfig(Builder builder) {
        this.outPath = builder.outPath;
        this.mode = builder.mode;
        this.author = builder.author;
        this.version = builder.version;
        this.packageName = builder.packageName;
        this.sqlDir = builder.sqlDir;
        this.entityDir = builder.entityDir;
        this.dtoDir = builder.dtoDir;
        this.voDir = builder.voDir;
        this.controllerDir = builder.controllerDir;
        this.serviceDir = builder.serviceDir;
        this.serviceImplDir = builder.serviceImplDir;
        this.mapperDir = builder.mapperDir;
        this.mapperXmlDir = builder.mapperXmlDir;
        this.apiJsDir = builder.apiJsDir;
        this.vue3Dir = builder.vue3Dir;
    }

    public String getOutPath() {
        return outPath;
    }

    public String getMode() {
        return mode;
    }

    public String getAuthor() {
        return author;
    }

    public String getVersion() {
        return version;
    }

    public String getPackageName() {
        return packageName;
    }

    public DirConfig getSqlDir() {
        return sqlDir;
    }

    public DirConfig getEntityDir() {
        return entityDir;
    }

    public DirConfig getDtoDir() {
        return dtoDir;
    }

    public DirConfig getVoDir() {
        return voDir;
    }

    public DirConfig getControllerDir() {
        return controllerDir;
    }

    public DirConfig getServiceDir() {
        return serviceDir;
    }

    public DirConfig getServiceImplDir() {
        return serviceImplDir;
    }

    public DirConfig getMapperDir() {
        return mapperDir;
    }

    public DirConfig getMapperXmlDir() {
        return mapperXmlDir;
    }

    public DirConfig getApiJsDir() {
        return apiJsDir;
    }

    public DirConfig getVue3Dir() {
        return vue3Dir;
    }

    public static class Builder {
        private String outPath;
        private String mode = ModeEnum.CAMEL_CASE.name();
        private String author;
        private String version;
        private String packageName;
        private DirConfig sqlDir = new DirConfig.Builder().dirName(GenTemplateEnum.TEMPLATE_MYSQL.getDir()).build();
        private DirConfig entityDir = new DirConfig.Builder().dirName(GenTemplateEnum.TEMPLATE_ENTITY.getDir()).build();
        private DirConfig dtoDir = new DirConfig.Builder().dirName(GenTemplateEnum.TEMPLATE_DTO.getDir()).build();
        private DirConfig voDir = new DirConfig.Builder().dirName(GenTemplateEnum.TEMPLATE_VO.getDir()).build();
        private DirConfig controllerDir = new DirConfig.Builder().dirName(GenTemplateEnum.TEMPLATE_CONTROLLER.getDir()).build();
        private DirConfig serviceDir = new DirConfig.Builder().dirName(GenTemplateEnum.TEMPLATE_SERVICE.getDir()).build();
        private DirConfig serviceImplDir = new DirConfig.Builder().dirName(GenTemplateEnum.TEMPLATE_SERVICE_IMPL.getDir()).build();
        private DirConfig mapperDir = new DirConfig.Builder().dirName(GenTemplateEnum.TEMPLATE_MAPPER.getDir()).build();
        private DirConfig mapperXmlDir = new DirConfig.Builder().dirName(GenTemplateEnum.TEMPLATE_MAPPER_XML.getDir()).build();
        private DirConfig vue3Dir = new DirConfig.Builder().dirName(GenTemplateEnum.TEMPLATE_VUE3.getDir()).build();
        private DirConfig apiJsDir = new DirConfig.Builder().dirName(GenTemplateEnum.TEMPLATE_API_JS.getDir()).build();

        public Builder outPath(String outPath) {
            this.outPath = outPath;
            return this;
        }

        public Builder mode(String mode) {
            this.mode = mode;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public Builder packageName(String packageName) {
            this.packageName = packageName;
            return this;
        }

        public Builder sqlDir(DirConfig sqlDir) {
            this.sqlDir = sqlDir;
            return this;
        }

        public Builder entityDir(DirConfig entityDir) {
            this.entityDir = entityDir;
            return this;
        }

        public Builder dtoDir(DirConfig dtoDir) {
            this.dtoDir = dtoDir;
            return this;
        }

        public Builder voDir(DirConfig voDir) {
            this.voDir = voDir;
            return this;
        }

        public Builder controllerDir(DirConfig controllerDir) {
            this.controllerDir = controllerDir;
            return this;
        }

        public Builder serviceDir(DirConfig serviceDir) {
            this.serviceDir = serviceDir;
            return this;
        }

        public Builder serviceImplDir(DirConfig serviceImplDir) {
            this.serviceImplDir = serviceImplDir;
            return this;
        }

        public Builder mapperDir(DirConfig mapperDir) {
            this.mapperDir = mapperDir;
            return this;
        }

        public Builder mapperXmlDir(DirConfig mapperXmlDir) {
            this.mapperXmlDir = mapperXmlDir;
            return this;
        }

        public Builder apiJsDir(DirConfig apiJsDir) {
            this.apiJsDir = apiJsDir;
            return this;
        }

        public Builder vue3Dir(DirConfig vue3Dir) {
            this.vue3Dir = vue3Dir;
            return this;
        }

        public GenTemplateConfig build() {
            return new GenTemplateConfig(this);
        }
    }
}
