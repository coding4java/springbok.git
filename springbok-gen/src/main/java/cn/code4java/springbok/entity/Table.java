package cn.code4java.springbok.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @ClassName TableColumn
 * @Description: 表
 * @Author fengwensheng
 * @Date 2024/7/8
 * @Version V2.0.0
 **/
@Data
@TableName(value = "sys_table")
public class Table {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer tableId;
    /**
     * 表名
     */
    private String tableName;
    /**
     * 模式
     * CAMEL_CASE：小驼峰，PASCAL_CASE：大驼峰，UNDERLINE：下划线
     */
    private String mode;
    /**
     * 类名
     */
    private String className;
    /**
     * 表描述
     */
    private String tableComment;
    /**
     * 作者
     */
    private String author;
    /**
     * 生成路径
     */
    private String outPath;
    /**
     * 包路径
     */
    private String packageName;
}
