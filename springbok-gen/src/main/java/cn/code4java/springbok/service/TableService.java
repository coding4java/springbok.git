package cn.code4java.springbok.service;

import cn.code4java.springbok.dto.TableDTO;
import cn.code4java.springbok.entity.Table;
import cn.code4java.springbok.vo.TableVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ClassName TableService
 * @Description: 服务类
 * @Author xiehuangbao
 * @Date 2024/07/09
 * @Version 2.0.0
 **/
public interface TableService extends IService<Table> {
    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    Page<Table> pageTable(TableDTO params);

    /**
     * 新增表
     *
     * @param tableDTO
     * @return
     */
    boolean addTable(TableDTO tableDTO);

    /**
     * 修改表
     *
     * @param tableDTO
     * @return
     */
    boolean updateTable(TableDTO tableDTO);

    /**
     * 删除表
     *
     * @param id
     * @return
     */
    boolean deleteTable(Integer id);

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    TableVO selectTableById(Integer id);

    /**
     * 生成代码zip压缩包
     *
     * @param tableId
     * @param response
     * @return
     */
    void generateCode(Integer tableId, HttpServletResponse response) throws Exception;
}
