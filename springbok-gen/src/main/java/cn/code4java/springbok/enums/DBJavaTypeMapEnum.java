package cn.code4java.springbok.enums;

import cn.code4java.springbok.exception.BusinessException;
import cn.code4java.springbok.exception.ExceptionEnum;

/**
 * @ClassName JavaSQLTypeMapEnum
 * @Description: 数据表与Java类型映射枚举
 * @Author fengwensheng
 * @Date 2024/7/8
 * @Version V2.0.0
 **/
public enum DBJavaTypeMapEnum {

    VARCHAR("varchar", "String", "java.lang"),
    TEXT("text", "String", "java.lang"),
    TINYINT("tinyint", "Boolean", "java.lang"),
    INT("int", "Integer", "java.lang"),
    INTEGER("integer", "Integer", "java.lang"),
    BIGINT("bigint", "Long", "java.lang"),
    DATE("date", "Date", "java.util"),
    DATETIME("datetime", "Date", "java.util"),
    FLOAT("float", "Float", "java.lang"),
    DOUBLE("double", "Double", "java.lang"),
    DECIMAL("decimal", "BigDecimal", "java.math");

    private String dbType;
    private String javaType;
    private String packageName;

    DBJavaTypeMapEnum(String dbType, String javaType, String packageName) {
        this.dbType = dbType;
        this.javaType = javaType;
        this.packageName = packageName;
    }

    public String getDbType() {
        return dbType;
    }

    public String getJavaType() {
        return javaType;
    }

    public String getPackageName() {
        return packageName;
    }

    public static DBJavaTypeMapEnum getDBJavaTypeMapEnum(String dbType){
        for (DBJavaTypeMapEnum dbJavaTypeMapEnum : DBJavaTypeMapEnum.values()) {
            if (dbJavaTypeMapEnum.getDbType().equals(dbType)) {
                return dbJavaTypeMapEnum;
            }
        }
        throw new BusinessException(ExceptionEnum.BUSINESS_PARAM_ERROR, "数据库类型错误");
    }
}
