package cn.code4java.springbok.enums;

/**
 * @ClassName ModeEnum
 * @Description: 命名方式枚举
 * @Author fengwensheng
 * @Date 2024/7/8
 * @Version V2.0.0
 **/
public enum ModeEnum {
    /**
     * 大驼峰
     */
    PASCAL_CASE,
    /**
     * 小驼峰
     */
    CAMEL_CASE,
    /**
     * 下划线
     */
    UNDERLINE;

    public static ModeEnum getModeEnum(String name) throws Exception {
        ModeEnum modeEnum = ModeEnum.valueOf(name);
        if (modeEnum == null) {
            throw new Exception("不存在" + name + "的ModeEnum值");
        }
        return modeEnum;
    }
}
