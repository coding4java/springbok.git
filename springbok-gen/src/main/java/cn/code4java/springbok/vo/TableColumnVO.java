package cn.code4java.springbok.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import java.lang.Boolean;
import java.lang.String;
import java.lang.Integer;

/**
 * @ClassName TableColumnVO
 * @Description: 生成表字段
 * @Author xiehuangbao
 * @Date 2024/07/09
 * @Version 2.0.0
 **/
@Data
public class TableColumnVO {

    /**
     * id
     */
    @ExcelProperty("id")
    private Integer tableColumnId;
    /**
     * 表id
     */
    @ExcelProperty("表id")
    private Integer tableId;
    /**
     * 列名
     */
    @ExcelProperty("列名")
    private String columnName;
    /**
     * 列类型
     */
    @ExcelProperty("列类型")
    private String columnType;
    /**
     * 列长度
     */
    @ExcelProperty("列长度")
    private Integer columnLength;
    /**
     * 字段名
     */
    @ExcelProperty("字段名")
    private String fieldName;
    /**
     * 字段类型
     */
    @ExcelProperty("字段类型")
    private String fieldType;
    /**
     * 列描述
     */
    @ExcelProperty("列描述")
    private String columnComment;
    /**
     * 是否主键
     */
    @ExcelProperty("是否主键")
    private Boolean primaryKey;
    /**
     * 是否为空
     */
    @ExcelProperty("是否为空")
    private Boolean isNull;
}
