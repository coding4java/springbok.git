import cn.code4java.springbok.SpringbokApplication;
import cn.code4java.springbok.entity.SysUser;
import cn.code4java.springbok.service.SysUserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @ClassName TestDemo
 * @Description: TestDemo
 * @Author fengwensheng
 * @Date 2024/3/19
 * @Version V1.0
 **/
@SpringBootTest(classes = SpringbokApplication.class)
public class TestDemo {

    @BeforeEach
    void beforeEach() {
        System.out.println("------------------- before -------------------");
    }

    @AfterEach
    void afterEach() {
        System.out.println(" ------------------- after -------------------");
    }

    @Autowired
    SysUserService sysUserService;

    @Test
    void listSysUser() {
        List<SysUser> list = sysUserService.list();
        list.stream().forEach(sysUser -> {
            System.out.println(sysUser.getNickName());
        });
    }
}
