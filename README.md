<p align="center">
	<img alt="logo" src="https://gitee.com/coding4java/springbok/raw/dev/image/logo.png">
</p>
<h4 align="center">基于SpringBoot开发的零售业务中台框架</h4>
<p align="center">
 <img src="https://img.shields.io/badge/Licence-Apache2.0-green.svg?style=flat" />
 <img src="https://img.shields.io/badge/Spring%20Boot-2.7-blue.svg" alt="Downloads">
 <img src="https://img.shields.io/badge/MyBatisPlus-3.4.2-blue.svg"/>
 <img src="https://img.shields.io/badge/Vue-3.4-blue.svg" alt="Downloads">
</p>

## ⭐项目介绍

基于Spring Boot2.7、Mybatis-Plus、Sa-Token，前端基于Vue3、Element-Plus、Vite，移动端基于Uni-App、TypeScript的零售业务中台框架，简洁、灵活、结构清晰，支持多门店多渠道销售、管理，遵循Apache2.0协议，完全开源，可免费用于个人或商业项目，点击上方Star，关注更新

### 前端项目

[https://gitee.com/coding4java/springbok-admin-vue3.git](https://gitee.com/coding4java/springbok-admin-vue3.git)

### 小程序项目

[https://gitee.com/coding4java/springbok-shop-uniapp.git](https://gitee.com/coding4java/springbok-shop-uniapp.git)

## 📔文档

[《springbok 开发文档》](https://www.yuque.com/fengwensheng-ot86q/rm6qf3?#)

## 分支介绍

| 分支   | 说明     |
| ------ | -------- |
| master | 主分支   |
| dev    | 开发分支 |

## 技术栈

### 后端

| 框架            | 版本    | 描述                |
| --------------- | ------- | ------------------- |
| JDK             | 1.8     | java 软件开发工具包 |
| Spring Boot     | 2.7.3   | 应用快速开发框架    |
| Spring MVC      | 5.3.22  | MVC 框架            |
| Sa-Token        | 1.37.0  | 轻量级权限认证框架  |
| MySQL           | >=5.7   | 数据库              |
| Mybatis-Plus    | 3.4.2   | Mybatis 增强工具    |
| Redis           | >=5.0   | Key-Value 数据库    |
| Lombok          | 1.18.24 | Java 类库           |
| Hutool          | 5.8.3   | Java 工具包         |
| Knife4j         | 2.0.8   | Swagger 增强        |
| Jackson         | 2.13.3  | JSON 工具库         |
| Quartz          | 2.7.3   | 定时任务调度框架    |
| Velocity        | 2.0     | Java 模板引擎       |
| SpringBoot Test | 2.7.3   | 单元测试库          |

### 前端

| 框架                 | 版本    | 描述                    |
| -------------------- | ------- | ----------------------- |
| Vue                  | 3.4.15  | 渐进式JavaScript框架    |
| Vite                 | 5.0.11  | 开发与构建工具          |
| TypeScript           | 5.3.0   | JavaScript类型的超集    |
| Element-Plus         | 2.5.5   | UI组件库                |
| Pinia                | 2.1.7   | Vue状态管理库           |
| Pinia-Plugin-Persist | 1.0.0   | Pinia持久化存储插件     |
| Vue Router           | 4.2.5   | Vue路由                 |
| Axios                | 1.6.7   | 基于Promise的网络请求库 |
| Echarts              | 5.4.3   | 可视化图表库            |
| Dayjs                | 1.11.10 | 日期操作工具库          |
| Wangeditor           | 5.1.23  | 富文本编辑器            |

### 移动端

| 框架                        | 版本   | 描述                   |
| --------------------------- | ------ | ---------------------- |
| Vue                         | 3.2.47 | 渐进式JavaScript框架 |
| Uni-App                     | 3.0.0  | 多端跨平台框架         |
| Uni-UI                      | 1.5.0  | uni-app组件库         |
| TypeScript                  | 5.1.6  | JavaScript类型的超集  |
| Pinia                       | 2.0.27 | Vue状态管理库         |
| Pinia-Plugin-Persistedstate | 3.2.0  | Pinia持久化存储插件   |

## 项目结构

```lua
springbok
├── springbok-common        -- 通用模块（必须）
├── springbok-core          -- 核心模块（必须）
├── springbok-system        -- 系统模块（必须）
├── springbok-system-api    -- 系统模块api（必须）
├── springbok-erp           -- ERP模块（必须）
├── springbok-erp-api       -- ERP模块api（必须）
├── springbok-mall          -- 零售模块（必须）
├── springbok-mall-api      -- 零售模块api（必须）
├── springbok-quartz        -- 定时任务模块（必须）
├── springbok-gen           -- 代码生成模块（必须）
├── springbok-monitor       -- 应用监控服务（可选）
├── springbok-mq            -- 消息模块（可选）
├── springbok-test          -- 示例，仅做示例用（可选）
└── springbok-test-api      -- 示例api，仅做示例用（可选）
```

## 项目截图

### 系统

<table>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/首页.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/菜单管理.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/角色管理.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/岗位管理.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/用户管理.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/应用管理.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/门店管理.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/字典管理.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/操作日志.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/定时任务.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/数据库监控.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/接口文档.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/应用监控.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/代码生成.png"/></td>
    </tr>
</table>

### 零售

<table>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/营销分类.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/规格管理.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/基础单位.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/基础商品.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/销售商品.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/渠道商品.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/销售订单.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/会员管理.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/标签管理.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/专题管理.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/广告管理.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/优惠券管理.png"/></td>
    </tr>
</table>

### ERP

<table>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/库存查询.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/盘点订单.png"/></td>
    </tr>
    <tr>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/采购出库单.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/采购入库单.png"/></td>
    </tr>
</table>

### 其他

<table>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/暗夜模式1.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/暗夜模式2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/其他主题1.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/其他主题2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/水印1.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/水印2.png"/></td>
    </tr>
</table>

## 微信小程序

<table>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_登录.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_我的.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_首页.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_首页2.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_分类.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_购物车.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_商品.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_商品2.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_商品3.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_结算.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_支付.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_支付2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_订单物流.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_订单列表.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_地址管理.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_门店.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_个人信息.png"/></td>
        <td><img src="https://gitee.com/coding4java/springbok/raw/master/image/小程序_专题.png"/></td>
    </tr>
</table>

## 开源协议

**本开源软件遵循 [Apache 2.0 协议](https://www.apache.org/licenses/LICENSE-2.0.html) ，100%开源，可免费作为个人或商业使用**
